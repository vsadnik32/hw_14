<?php

class Publication 

{
	public $id= 0;
	public $caption = '';
	public $text = '';
	public $full_text = '';
		

	public function __construct($id,$caption,$text,$full_text)
	{
		$this->id=$id;
		$this->caption = $caption;
		$this->text = $text;
		$this->full_text = $full_text;

		
	}

	
	public static  function create($id, PDO $pdo)
	{
		
		$sql = 'SELECT * FROM members WHERE id=:id';
		$pdo_bag = $pdo->prepare($sql);
		$pdo_bag->bindValue(':id', $id);
		$pdo_bag->execute();
		$bag_arr = $pdo_bag->fetch();
	
		$bag = new self($bag_arr['caption'], $bag_arr['text'], $bag_arr['full_text'],$bag_arr['author'],$bag_arr['source']);
	}
	
}