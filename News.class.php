<?php
class News extends Publication
{
	public $source = '';

    function __construct($id,$caption,$text,$full_text,$source)
	    {
	        parent::__construct($id,$caption,$text,$full_text);
	        $this->source = $source;
	    }
    public function getAuthor()
    	{
       	    return $this->source;
        }
}