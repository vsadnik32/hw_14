 <?php
 // создаем таблицу members
require_once 'db_connection.php';


try{
	$sql = 'CREATE TABLE members(
			id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
			caption VARCHAR(255),
			text VARCHAR(255),
			full_text VARCHAR(255),
			author VARCHAR(255),
			source VARCHAR(255),
			publication_type VARCHAR(255)

 
	)DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
	$pdo->exec($sql);

}catch(PDOException $e){
	echo 'Не удалось создать таблицу quotes ' . $e->getMessage();
}