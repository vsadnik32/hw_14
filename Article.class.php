<?php
class Article extends Publication
{
    public $author = '';

    function __construct($id,$caption,$text,$full_text,$author)
    {
        parent::__construct($id,$caption,$text,$full_text);
        $this->author = $author;
    }
    public function getAuthor()
    {
        return $this->author;
    }

}